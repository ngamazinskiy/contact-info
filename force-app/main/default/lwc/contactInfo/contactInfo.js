import  { LightningElement, api, wire, track } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import PHOTO_UR from '@salesforce/schema/Contact.Photo_URL__c';
import PHONE from '@salesforce/schema/Contact.Phone';
import NAME from '@salesforce/schema/Contact.Name';
import EMAIL from '@salesforce/schema/Contact.Email';
import MAILING_STREET from '@salesforce/schema/Contact.MailingStreet';
import MAILING_CITY from '@salesforce/schema/Contact.MailingCity';
import MAILING_STATE from '@salesforce/schema/Contact.MailingState';
import MAILING_POSTAL_CODE from '@salesforce/schema/Contact.MailingPostalCode';
import MAILING_COUNTRY from '@salesforce/schema/Contact.MailingCountry';

const FIELDS = [
    PHOTO_UR, PHONE, NAME, EMAIL,
    MAILING_STREET, MAILING_CITY, MAILING_STATE, MAILING_POSTAL_CODE, MAILING_COUNTRY
];

const AVATAR_URL = '/_slds/images/themes/lightning_blue/lightning_blue_profile_avatar_200.png';
export default class ContactInfo extends LightningElement {
    @api recordId;
    @track contact;
    @track photoUrl;

    @wire(getRecord, {recordId: '$recordId', fields: FIELDS})
    getContactFormData({ data }) {
        if (data) {
            this.photoUrl = getFieldValue(data, PHOTO_UR) ? getFieldValue(data, PHOTO_UR) : AVATAR_URL;

            this.contact = {
                email: getFieldValue(data, EMAIL),
                name: getFieldValue(data, NAME),
                phone: getFieldValue(data, PHONE),
                fullAddress: this.buildAddress(data)
            }
        }
    }

    buildAddress(data) {
        let addressParts = [
            getFieldValue(data, MAILING_STREET),
            getFieldValue(data, MAILING_CITY),
            getFieldValue(data, MAILING_STATE),
            getFieldValue(data, MAILING_POSTAL_CODE),
            getFieldValue(data, MAILING_COUNTRY)
        ];

        addressParts = addressParts.filter(part => part !== null && part !== undefined && part !== '');

        return addressParts.join(', ');
    }

    handleImageError(event) {
        event.target.src = AVATAR_URL;
    }

    handlerCopyText(event) {
        const textToCopy = event.currentTarget.title;
        navigator.clipboard.writeText(textToCopy)
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Copied',
                        message: textToCopy,
                        variant: 'success'
                    })
                );
            })
    }
}